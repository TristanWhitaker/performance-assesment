package com.example.student.preformanceassesment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ItemFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (MainActivity)activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_post, container, false);
        //viewPager = (ViewPager) findViewById(R.id.viewPager);

        TextView i1 = (TextView)layoutView.findViewById(R.id.reddittext1);
        i1.setText("Name: " + activity.todoPosts.get(activity.currentItems).title);

        TextView i2 = (TextView)layoutView.findViewById(R.id.reddittext2);
        i2.setText("Name: " + activity.todoPosts.get(activity.currentItems).title);

        TextView i3 = (TextView)layoutView.findViewById(R.id.reddittext3);
        i3.setText("Name: " + activity.todoPosts.get(activity.currentItems).title);

        TextView i4 = (TextView)layoutView.findViewById(R.id.reddittext4);
        i4.setText("Name: " + activity.todoPosts.get(activity.currentItems).title);

        return layoutView;
    }
}