package com.example.student.preformanceassesment;

public class ToDoPost {
    public String title;
    public String dateAdded;
    public String dateDue;
    public String category;


    public ToDoPost(String title, String dateAdded,String dateDue, String category ) {
        this.title = title;
        this.dateAdded = dateAdded;
        this.dateDue = dateDue;
        this.category = category;
    }
}