package com.example.student.preformanceassesment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ToDoFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (MainActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_to_do_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ToDoPost i1 = new ToDoPost("ABC", "Janurary", "May", "letters");
        ToDoPost i2 = new ToDoPost("DEF", "February", "June", "mo letters");
        ToDoPost i3 = new ToDoPost("GHI", "March", "July", "even mo letters");
        ToDoPost i4 = new ToDoPost("JKL", "April", "August", "too many letters" );

        activity.todoPosts.add(i1);
        activity.todoPosts.add(i2);
        activity.todoPosts.add(i3);
        activity.todoPosts.add(i4);

        ToDoAdapter adapter = new ToDoAdapter(activityCallback, activity.todoPosts);
        recyclerView.setAdapter(adapter);

        return view;
    }
}
